//
//  ViewController.swift
//  homeWorkOneFominUnicornovich
//
//  Created by  Евгений on 22.09.2018.
//  Copyright © 2018 LosAnatoly Inc. All rights reserved.
//
//2 экрана для того, что бы увидеть методы viewWillDisappear и viewDidDisappear


import UIKit

class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        funcDescription(funcName: #function)
    }
    
    override func viewWillLayoutSubviews() {
        funcDescription(funcName: #function)
    }
    
    override func viewDidLayoutSubviews() {
        funcDescription(funcName: #function)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        funcDescription(funcName: #function)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        funcDescription(funcName: #function)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        funcDescription(funcName: #function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        funcDescription(funcName: #function)
    }
    
    func funcDescription (funcName: String) {
        print("ViewController called: \(funcName)")
    }
    
    
}

