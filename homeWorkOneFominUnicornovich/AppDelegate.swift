//
//  AppDelegate.swift
//  homeWorkOneFominUnicornovich
//
//  Created by  Евгений on 22.09.2018.
//  Copyright © 2018 LosAnatoly Inc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
         stateDidChange(from: "Not Running", to: application.applicationState.string, functionName: #function)
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        return true
    }

    
    
    func applicationWillResignActive(_ application: UIApplication) {
         stateDidChange(from: application.applicationState.string, to: UIApplicationState.inactive.string, functionName: #function)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        stateDidChange(from: UIApplicationState.inactive.string, to: application.applicationState.string, functionName: #function)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        stateDidChange(from: application.applicationState.string, to: UIApplicationState.inactive.string, functionName: #function)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        stateDidChange(from: UIApplicationState.inactive.string , to: application.applicationState.string, functionName: #function)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        stateDidChange(from: application.applicationState.string, to: "Terminated", functionName: #function)
    }
    
    func stateDidChange (from: String, to: String, functionName: String) {
        print("Application moved from \(from) to \(to): ", functionName)
    }
    
}

fileprivate extension UIApplicationState {
    
    var string: String {
        get {
            switch self {
            case .active:
                return "active"
            case .inactive:
                return "inactive"
            case .background:
                return "background"
            }
        }
    }
    
}
